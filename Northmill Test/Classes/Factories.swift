//
//  ArticleCategoryFactories.swift
//  Northmill Test
//
//  Created by Krystian Sęk on 23/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation

// Usually I create factories for objects that can be stored in database. It makes database change easy to do in whole project.
protocol ArticleCategoryFactory {
    func make(with categoryId: Int, and categoryName: String) -> ArticleCategory
}


class SimpleArticleCategoryFactory: ArticleCategoryFactory {

    private struct SimpleArticleCategory: ArticleCategory {
        let categoryId: Int
        let categoryName: String
    }
    
    func make(with categoryId: Int, and categoryName: String) -> ArticleCategory {
        return SimpleArticleCategory(categoryId: categoryId, categoryName: categoryName)
    }
}
