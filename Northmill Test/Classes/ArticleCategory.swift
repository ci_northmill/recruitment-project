//
//  ArticleCategory.swift
//  Northmill Test
//
//  Created by Krystian Sęk on 23/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation

protocol ArticleCategory {
    var categoryId: Int { get }
    var categoryName: String { get }
}
