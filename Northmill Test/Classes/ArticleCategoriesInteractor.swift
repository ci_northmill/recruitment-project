//
//  ArticleCategoriesInteractor.swift
//  Northmill Test
//
//  Created by Krystian Sęk on 23/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation

protocol ArticleCategoriesPresenter {
    func present(_ categories: [ArticleCategory])
}

class ArticleCategoriesInteractor {
    private let datasource: ArticleCategoriesDatasource
    private let presenter: ArticleCategoriesPresenter
    
    init(datasource: ArticleCategoriesDatasource = ArticleCategoriesRESTDatasource(), presenter: ArticleCategoriesPresenter) {
        self.datasource = datasource
        self.presenter = presenter
    }
    
    
    func reloadData() {
        datasource.getCategories { categories in
            guard let downloadedCategories = categories else { return }
            self.presenter.present(downloadedCategories)
        }
    }
    
    
    func addCategory(with name: String, completion: @escaping (_ success: Bool) -> ()) {
        datasource.addCategory(with: name) { success in
            completion(success)
            // unfortunately there is no information about created instance in backend response,
            // so in order to have it (we can use name, but there is still no "Id" value) I need to reload data again.
            self.reloadData()
        }
    }
}
