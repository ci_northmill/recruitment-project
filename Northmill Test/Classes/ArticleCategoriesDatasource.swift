//
//  ArticleCategoriesDatasource.swift
//  Northmill Test
//
//  Created by Krystian Sęk on 23/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import Foundation
import Alamofire

protocol ArticleCategoriesDatasource {
    func getCategories(completion: @escaping ((_ categories: [ArticleCategory]?) -> ()))
    func addCategory(with name: String, completion: @escaping (_ success: Bool) -> ())
}


class ArticleCategoriesRESTDatasource: ArticleCategoriesDatasource {
    let factory: ArticleCategoryFactory
    let sessionManager: SessionManager
    let REQUESTS_TIMEOUT: TimeInterval = 30
    
    init(factory: ArticleCategoryFactory = SimpleArticleCategoryFactory()) {
        self.factory = factory
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = REQUESTS_TIMEOUT
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
}


/**
 For the sake of simplicity I decided to make these calls directly from REST datasource of categories (only because this test project is simple).
 Normally I would move it to dedicated or shared network manager designed for project use.
 */
extension ArticleCategoriesRESTDatasource {
    private var apiURLToCategories: String { return "https://apitest-northmill.azurewebsites.net/api/ArticleCategory" }
    private var apiUser: String { return "api" }
    private var apiPassword: String { return "tester" }
    
    func getCategories(completion: @escaping ((_ categories: [ArticleCategory]?) -> ())) {
        sessionManager.request(apiURLToCategories) .authenticate(user: apiUser, password: apiPassword) .validate() .responseJSON { response in
            switch response.result {
            case .success:
                guard let json = response.result.value as? [[String : AnyObject]] else { completion(nil); return }
                completion(self.parsed(json))
            case .failure(_):
                completion(nil)
            }
        }
    }
    
    
    func addCategory(with name: String, completion: @escaping (Bool) -> ()) {
        sessionManager.request(apiURLToCategories, method: .post, parameters: [:], encoding: name, headers: ["Content-Type":"application/json"])
            .authenticate(user: apiUser, password: apiPassword)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    completion(true)
                case .failure(_):
                    completion(false)
                }
        }
    }
    
    
    private func parsed(_ categoriesToParse: [[String : AnyObject]]) -> [ArticleCategory] {
        var parsedCategories = [ArticleCategory]()
        for categoryDictionary in categoriesToParse {
            guard let categoryId = categoryDictionary["Id"] as? Int else { continue }
            guard let categoryName = categoryDictionary["Name"] as? String else { continue }
            parsedCategories.append(factory.make(with: categoryId, and: categoryName))
        }
        return parsedCategories
    }
}


extension String: ParameterEncoding {
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = "\"\(self)\"".data(using: .utf8, allowLossyConversion: false)
        return request
    }
}
