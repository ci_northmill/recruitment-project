//
//  ViewController.swift
//  Northmill Test
//
//  Created by Krystian Sęk on 23/12/2016.
//  Copyright © 2016 Krystian Sęk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var cachedCategories = [ArticleCategory]()
    var refreshControl = UIRefreshControl()
    var categoriesInteractor: ArticleCategoriesInteractor!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoryNameTextField: UITextField!
    @IBOutlet weak var addCategoryButton: UIButton!
    @IBOutlet weak var addingCategoryLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var categoryNameTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var addCategoryButtonBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoriesInteractor = ArticleCategoriesInteractor(presenter: self)
        registerForKeyboardNotifications()
        prepareRefreshControll()
        makeCellsResizable()
        reloadDataInView()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func pulledToRefresh() {
        reloadDataInView()
    }
    
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: .UIKeyboardWillHide, object: nil)
    }
    
    
    func keyboardWillShow(notification: Notification) {
        let height = notification.keyboardFrame().size.height
        updateViewLayout(forKeyboardHeight: height)
    }
    
    func keyboardDidShow(notification: Notification) {
        tableView.scrollToRow(at: IndexPath(row: cachedCategories.count - 1, section: 0), at: .bottom, animated: true)
    }
    
    func keyboardWillBeHidden() {
        updateViewLayout(forKeyboardHeight: 0)
    }
    
    
    private func updateViewLayout(forKeyboardHeight keyboardHeight: CGFloat) {
        addCategoryButtonBottomConstraint.constant = keyboardHeight
        categoryNameTopConstraint.constant = keyboardHeight == 0 ? 0 : -categoryNameTextField.bounds.size.height
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    private func reloadDataInView() {
        categoriesInteractor.reloadData()
    }
    
    
    private func prepareRefreshControll() {
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing list")
        refreshControl.addTarget(self, action: #selector(pulledToRefresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    
    private func makeCellsResizable() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
    }
    
    
    func setUITo(downloadState: Bool) {
        addCategoryButton.isEnabled = !downloadState
        addingCategoryLoadingIndicator.isHidden = !downloadState
        categoryNameTextField.isEnabled = !downloadState
    }
    
    
    @IBAction func didTapAddCategoryButton() {
        if categoryNameTextField.isFirstResponder {
            guard let newCategoryName = categoryNameTextField.text else { return }
            setUITo(downloadState: true)
            categoriesInteractor.addCategory(with: newCategoryName) { success in
                self.categoryNameTextField.text = nil
                self.setUITo(downloadState: false)
                self.categoryNameTopConstraint.constant = 0
            }
        } else {
            addCategoryButton.isEnabled = false
            categoryNameTextField.becomeFirstResponder()
            categoryNameTopConstraint.constant = -categoryNameTextField.bounds.size.height
        }
    }
    
    @IBAction func didChangeCategoryName() {
        addCategoryButton.isEnabled = (categoryNameTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "")
    }
}


extension ViewController: ArticleCategoriesPresenter {
    func present(_ categories: [ArticleCategory]) {
        refreshControl.endRefreshing()
        cachedCategories = categories
        tableView.reloadData()
        setUITo(downloadState: false)
        tableView.scrollToRow(at: IndexPath(row: cachedCategories.count - 1, section: 0), at: .bottom, animated: true)
    }
}


extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cachedCategories.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")! as UITableViewCell
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.textColor = .darkGray
        cell.textLabel?.text = cachedCategories[indexPath.row].categoryName
        return cell
    }
}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


extension Foundation.Notification {
    func keyboardFrame() -> CGRect {
        if let size = self.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            return size.cgRectValue
        } else {
            return CGRect.zero
        }
    }
}
