# Installation #

Project makes use of external framework called _[Alamofire](https://github.com/Alamofire/Alamofire)_. It's managed by means of _[Carthage](https://github.com/Carthage/Carthage)_ dependency manager.
Please remember to run

`carthage bootstrap`

after cloning repository.

### Task description ###

```
This is a REST API published on Azure:

https://apitest-northmill.azurewebsites.net/api

Write a simple iOS app using whichever framework and software you prefer which has the following functionality:

1.App displays available article categories inside drop-down list

GET article categories API is described here:

https://apitest_northmill.azurewebsites.net/Help/Api/GET-api-ArticleCategory

2. Give user option to add new categories. Category consists of single field: Name

Send HTTP Post as described here:

https://apitest-northmill.azurewebsites.net/Help/Api/POST-api-ArticleCategory

Warning: you might experience varying time response when addingnew categories Your app should handle possible 1-30s delays when waiting for API to respond.

3. User of the app should be able to see new added categories in dropdown list.

Credentials used to connect to REST API:
- BasicAuth with login and password sent in HTTPS header (https://en.wikipedia.org/wiki/Basic_access_authentication)
- user name api
- password tester

Please submit your solution within 3 calendar days from the moment you finish this test via e-mail ina single ZIP file to sent to info@northmill.pl

Your solution will be evaluated not by the Codility, but by the Northmill IT team.

```